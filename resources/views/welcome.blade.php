<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Resource mangement system</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />


    <style>
         body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }
        .antialiased {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale
        }
        .center {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            position: relative;
            text-align: center
        }
        .centertext {
            letter-spacing: 5px;
            font-weight: bolder;
            font-size: 30px;
            text-transform: uppercase;
        }
        </style>

</head>

<body class="antialiased">
    <div class=" center">
        <div class="content">
            <h1 class="centertext">
                Resource mangement system
            </h1>
        </div>
    </div>
</body>

</html>
